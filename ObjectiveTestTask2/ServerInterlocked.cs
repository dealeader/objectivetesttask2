﻿namespace ObjectiveTestTask2;

public static class ServerInterlocked
{
    private static int _count;

    public static int GetCount()
    {
        return _count;
    }

    public static void AddToCount(int value)
    {
        Interlocked.Add(ref _count, value);
    }
}