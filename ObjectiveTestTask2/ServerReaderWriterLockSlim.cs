﻿namespace ObjectiveTestTask2;

public static class ServerReaderWriterLockSlim
{
    private static int _count;
    private static ReaderWriterLockSlim _rwlock = new ReaderWriterLockSlim();

    public static int GetCount()
    {
        _rwlock.EnterReadLock();
        try
        {
            return _count;
        }   
        finally
        {
            _rwlock.ExitReadLock();
        }
    }

    public static void AddToCount(int value)
    {
        _rwlock.EnterWriteLock();
        try
        {
            _count += value;
        }   
        finally
        {
            _rwlock.ExitWriteLock();
        }
    }
}