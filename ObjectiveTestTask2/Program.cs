﻿using ObjectiveTestTask2;

var tasks = new List<Task>(100);
var rnd = new Random();

Parallel.For(0, 100000, i =>
{
    if (i % 10 == 0)
    {
        ServerInterlocked.AddToCount(1);
        ServerReaderWriterLockSlim.AddToCount(1);
    }
    else
    {
        ServerInterlocked.GetCount();
        ServerReaderWriterLockSlim.GetCount();
    }
});

Console.WriteLine($"ServerInterlocked: {ServerInterlocked.GetCount()}");
Console.WriteLine($"ServerReaderWriterLockSlim: {ServerReaderWriterLockSlim.GetCount()}");